package adsgofast.com.adsgofast;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;

public class About extends AppCompatActivity {

    Button Bcheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_app);

        Bcheck = (Button) findViewById(R.id.Bcheck);
        Bcheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://adsgofast.blogspot.co.id/"));
                startActivity(intent);
                }

            });
    }
    public void onButtonClick(View view){
        if(view.getId() == R.id.Babout){
            Intent about = new Intent(About.this, Maps.class);
            startActivity(about);
        }
    }
}
