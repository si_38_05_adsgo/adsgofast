package adsgofast.com.adsgofast;
//Fifth Commit
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper helper = new DatabaseHelper(this);

    NotificationCompat.Builder notif;
    private static final int uniqueID = 45612;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        notif = new NotificationCompat.Builder(this);
        notif.setAutoCancel(true);
    }

    public void onButtonClick(View v)
    {
        if(v.getId() == R.id.Blogin)
        {
            EditText user = (EditText)findViewById(R.id.TFuser);
            String userStr = user.getText().toString();
            EditText pass = (EditText)findViewById(R.id.TFpass);
            String passStr = pass.getText().toString();

            //Build the notification
            notif.setSmallIcon(R.drawable.logoag);
            notif.setTicker("Login in to AdsGO");
            notif.setWhen(System.currentTimeMillis());
            notif.setContentTitle("Login in to AdsGO");
            notif.setContentText("Click here to see our Location");

            Intent notifIntent = new Intent(MainActivity.this, Maps.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notifIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            notif.setContentIntent(pendingIntent);

            //Build notification and issues it
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.notify(uniqueID, notif.build());

            String password = helper.searchPass(userStr);
            if(userStr.equals("")&& passStr.equals(""))
            {
                Toast tmp = Toast.makeText(MainActivity.this, "Username and password cannot empty", Toast.LENGTH_SHORT);
                tmp.show();
            }

            else if(passStr.equals(password))
            {
                if(userStr.equals("muraf"))
                {
                    Intent masuk = new Intent(MainActivity.this, Driver_input_paket.class);
                    startActivity(masuk);
                }else{
                    Intent masuk = new Intent(MainActivity.this, Paket_static.class);
                    startActivity(masuk);
                }
            }
            else
            {
                Toast tmp = Toast.makeText(MainActivity.this, "Username and password wrong", Toast.LENGTH_SHORT);
                tmp.show();
            }
        }

        if(v.getId() == R.id.Bsignup)
        {
            Intent daftar = new Intent(MainActivity.this, Signup.class);
            startActivity(daftar);
        }

        if(v.getId() == R.id.Babout)
        {
            Intent about = new Intent(MainActivity.this, About.class);
            startActivity(about);
        }

    }
}
