package adsgofast.com.adsgofast;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Win 8 on 29-Apr-17.
 */

public class DatabaseHelper extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "adsgo.db";
    private static final String TABLE_NAME = "driver";
    private static final String COLUMN_USER = "user";
    private static final String COLUMN_PASS = "pass";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_NAME = "name";

    private static final String TABLE_NAME_PACKET = "paket";
    private static final String COLUMN_NAME_PACKET = "nama";
    private static final String COLUMN_POSITION = "posisi";
    private static final String COLUMN_SALARY = "gaji";
    private static final String COLUMN_UNIT = "unit";
    private static final String COLUMN_DISTANCE = "jarak";

    SQLiteDatabase db;

    private static final String TABLE_CREATE = "create table driver(user text not null , "+
    "pass text not null, email text not null ,name text not null);";

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null , DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
        this.db = db;

        String createTablePacket = "create table paket(nama text not null , "+
                "posisi text not null, gaji text not null ,unit text not null, jarak text not null);";
        db.execSQL(createTablePacket);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String query = "DROP TABLE IF EXIST "+TABLE_NAME;
        db.execSQL(query);
        this.onCreate(db);
        db.execSQL("DROP TABLE IF EXIST "+TABLE_NAME_PACKET);

    }

    public void regisDriver(Driver d)
    {
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        String query = "select * from driver";
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();

        values.put(COLUMN_USER, d.getUser());
        values.put(COLUMN_PASS, d.getPass());
        values.put(COLUMN_EMAIL, d.getEmail());
        values.put(COLUMN_NAME, d.getName());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public String searchPass(String user)
    {
        db = this.getReadableDatabase();
        String query = "select user,pass from "+TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        String a,b;
        b = "Username or password not registered";
        if(cursor.moveToFirst())
        {
            do{
                a = cursor.getString(0);
                b = cursor.getString(1);

                if(a.equals(user))
                {
                    b = cursor.getString(1);
                    break;
                }
            }
            while (cursor.moveToNext());
        }
        return b;
    }

    public boolean addDataPacket(String nama, String posisi, String gaji, String unit, String jarak)
    {
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_NAME_PACKET, nama);
        values.put(COLUMN_POSITION, posisi);
        values.put(COLUMN_SALARY, gaji);
        values.put(COLUMN_UNIT, unit);
        values.put(COLUMN_DISTANCE, jarak);

        long result = db.insert(TABLE_NAME_PACKET, null, values);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    public Cursor getListContents()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("SELECT * FROM " + TABLE_NAME_PACKET, null);
        return data;
    }




}
