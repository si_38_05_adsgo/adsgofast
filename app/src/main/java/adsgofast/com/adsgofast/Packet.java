package adsgofast.com.adsgofast;

/**
 * Created by Win 8 on 05-May-17.
 */

public class Packet {
    String nama, posisi, gaji, unit, jarak;

    public String getNama() { return nama; }

    public void setNama(String nama) { this.nama = nama; }

    public String getPosisi() { return posisi; }

    public void setPosisi(String posisi) { this.posisi = posisi; }

    public String getGaji() { return gaji; }

    public void setGaji(String gaji) { this.gaji = gaji; }

    public String getUnit() { return unit; }

    public void setUnit(String unit) { this.unit = unit; }

    public String getJarak() { return jarak; }

    public void setJarak(String jarak) { this.jarak = jarak; }
}
