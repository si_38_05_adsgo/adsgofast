package adsgofast.com.adsgofast;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Win 8 on 29-Apr-17.
 */

public class Signup extends Activity {
    DatabaseHelper helper = new DatabaseHelper(this);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
    }

    public void onSignUpClick(View v)
    {
        boolean valid = true;

        if(v.getId() == R.id.Bsignup)
        {
            EditText user = (EditText)findViewById(R.id.TFuser);
            EditText pass = (EditText)findViewById(R.id.TFpass);
            EditText email = (EditText)findViewById(R.id.TFemail);
            EditText name = (EditText)findViewById(R.id.TFname);

            String userStr = user.getText().toString();
            String passStr = pass.getText().toString();
            String emailStr = email.getText().toString();
            String nameStr = name.getText().toString();

            Driver d = new Driver();
            d.setUser(userStr);
            d.setPass(passStr);
            d.setEmail(emailStr);
            d.setName(nameStr);

            helper.regisDriver(d);

            Toast tmp = Toast.makeText(Signup.this, "Account created!", Toast.LENGTH_SHORT);
            tmp.show();

            Intent masuk = new Intent(Signup.this, MainActivity.class);
            //masuk.putExtra('Username', userStr);
            startActivity(masuk);

        }

    }

}
