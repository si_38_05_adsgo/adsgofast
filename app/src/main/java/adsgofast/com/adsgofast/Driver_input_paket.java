package adsgofast.com.adsgofast;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Driver_input_paket extends AppCompatActivity {

    DatabaseHelper myDb;
    Button Bsave, Bview;

    EditText nama, posisi, gaji, unit, jarak;
    String namaStr, posisiStr, gajiStr, unitStr, jarakStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_input_paket);

        nama = (EditText)findViewById(R.id.et_nama);
        posisi = (EditText)findViewById(R.id.et_posisi);
        gaji = (EditText)findViewById(R.id.et_gaji);
        unit = (EditText)findViewById(R.id.et_unit);
        jarak = (EditText)findViewById(R.id.et_jarak);

        Bsave = (Button)findViewById(R.id.Bsave);
        Bview = (Button)findViewById(R.id.Bview);

        myDb = new DatabaseHelper(this);

        Bview.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent masuk = new Intent(Driver_input_paket.this, Driver_product.class);
                startActivity(masuk);
            }
        });

        Bsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String namaEntry = nama.getText().toString();
                String posisiEntry = posisi.getText().toString();
                String gajiEntry = gaji.getText().toString();
                String unitEntry = unit.getText().toString();
                String jarakEntry = jarak.getText().toString();

                if(namaEntry.equals("")){
                    Toast.makeText(Driver_input_paket.this, "Packet name cannot empty", Toast.LENGTH_LONG).show();
                }else if(posisiEntry.equals("")){
                    Toast.makeText(Driver_input_paket.this, "Position ads cannot empty", Toast.LENGTH_LONG).show();
                }else if(gajiEntry.equals("")){
                    Toast.makeText(Driver_input_paket.this, "Salary cannot empty", Toast.LENGTH_LONG).show();
                }else if(unitEntry.equals("")){
                    Toast.makeText(Driver_input_paket.this, "Unit cannot empty", Toast.LENGTH_LONG).show();
                }else if(jarakEntry.equals("")){
                    Toast.makeText(Driver_input_paket.this, "Distance cannot empty", Toast.LENGTH_LONG).show();
                }else{
                    AddData(namaEntry, posisiEntry, gajiEntry, unitEntry, jarakEntry);
                }
            }
        });
    }

    public void AddData(String namaEntry, String posisiEntry, String gajiEntry, String unitEntry, String jarakEntry){
        boolean insertData = myDb.addDataPacket(namaEntry, posisiEntry, gajiEntry, unitEntry, jarakEntry);

        if(insertData == true){
            Toast.makeText(Driver_input_paket.this, "Data Packet Successfully Entered", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(Driver_input_paket.this, "Something went wrong :( ", Toast.LENGTH_LONG).show();
        }
    }

}
